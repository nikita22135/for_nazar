#pragma once
#include <string>
using namespace std;
class AbstractPush
{
public:
	explicit AbstractPush();
	void set_text(string s);
	void set_type(string t);
	string get_text();
	string get_type();
	virtual void getParametrs() = 0;
	virtual void findParametr(string s) = 0;
	virtual void filter() = 0;
	virtual bool getIsFilter() ;
protected:
	string _text;
	string _type;
	bool _isFilter = false;
private:
	
};

class LocationPush: public virtual AbstractPush
{
public:
	explicit LocationPush(long time, float x_coord, float y_coord);
	void getParametrs() override;
	void findParametr(string s) override;
	void filter() override;
private:
	float _x_coord;
	float _y_coord;
	float _checkX;
	float _checkY;
	int _radius;
	long _expiry_date;
	bool _isFilter = false;
	long _time;
};

class AgeSpecicPush : public virtual AbstractPush
{
public:
	explicit AgeSpecicPush(int age, long expiry_date);
	void getParametrs() override;
	void findParametr(string s) override;
	void filter() override;
private:
	int _age;
	int _checkAge;
	long _expiry_date;
	long _checkExpity_date;
};

class TechPush : public virtual AbstractPush
{
public:
	explicit TechPush(int os_version);
	void getParametrs() override;
	void findParametr(string s) override;
	void filter() override;
private:
	int _os_version;
	int _checkOs_version;
};

class LocationAgePush : public virtual AbstractPush
{
public:
	explicit LocationAgePush(float x_coord, float y_coord, int age);
	void getParametrs() override;
	void findParametr(string s) override;
	void filter() override;
private:
	float _x_coord;
	float _y_coord;
	float _checkX;
	float _checkY;
	int _radius;
	int _checkAge;
	int _age;
};

class GenderAgePush : public virtual AbstractPush
{
public:
	explicit GenderAgePush(int age, char gender);
	void getParametrs() override;
	void findParametr(string s) override;
	void filter() override;
private:
	int _checkAge;
	int _age;
	char _gender;
	char _checkGender;
};

class GenderPush : public virtual AbstractPush
{
public:
	explicit GenderPush(char gender);
	void getParametrs() override;
	void findParametr(string s) override;
	void filter() override;
private:
	char _gender;
	char _checkGender;
};