#include<iostream>
#include <string>
#include <cstdio>
#include "Push.h"
using namespace std;
//template < typename T>
long find_str(string s, string findingString, long value)
{
	size_t pos = s.find(findingString);
	pos += findingString.size();
	int end_pos = pos;
	while (isdigit(s[end_pos]))
		end_pos++;
	string sub_str = s.substr(pos + 1, end_pos);
	return atoi(sub_str.c_str());
}

int find_str(string s, string findingString, int value)
{
	size_t pos = s.find(findingString);
	pos += findingString.size();
	int end_pos = pos;
	while (isdigit(s[end_pos]))
		end_pos++;
	string sub_str = s.substr(pos + 1, end_pos);
	return atoi(sub_str.c_str());
}

float find_str(string s, string findingString)
{
	size_t pos = s.find(findingString);
	pos += findingString.size();
	int end_pos = pos;
	while ((isdigit(s[end_pos])) || (s[end_pos] == '.'))
		end_pos++;
	string sub_str = s.substr(pos + 1, end_pos);
	return atof(sub_str.c_str());
}

char find_str(string s)
{
	size_t pos = s.find("gender");
	pos += 7;
	return s[pos];
}
bool isSomethingFiltred = false;
int main(int argc, char* argv[])
{
	long time = 0;
	int age = 0;
	char gender;
	int os_version = 0;
	float x_coord = 0;
	float y_coord = 0;
	long N;
	string s;
	getline(cin, s);
	time = find_str(s, "time", time);
	age = find_str(s, "age", age);
	os_version = find_str(s, "os_version", os_version);
	x_coord = find_str(s, "x_coord");
	y_coord = find_str(s, "y_coord");
	gender = find_str(s);
	cin >> N;
	for (int i = 1; i <= N; i++)
	{
		int M;
		string pushType = "";
		cin >> M;
		getline(cin, pushType);
		AbstractPush* push;
		push = nullptr;
		if (static_cast<int>(pushType.find("LocationPush")) != -1)
		{
			push = new LocationPush(time, x_coord, y_coord);
			push->filter();
		}
		if (static_cast<int>(pushType.find("AgeSpecificPush")) != -1)
		{
			push = new AgeSpecicPush(age, time);
			push->filter();
		}
		if (static_cast<int>(pushType.find("TechPush")) != -1)
		{
			push = new TechPush(os_version);
			push->filter();
		}
		if (static_cast<int>(pushType.find("LocationAgePush")) != -1)
		{
			push = new LocationAgePush(x_coord, y_coord, age);
			push->filter();
		}
		if (static_cast<int>(pushType.find("GenderAgePush")) != -1)
		{
			push = new GenderAgePush(age, gender);
			push->filter();
		}
		if (static_cast<int>(pushType.find("GenderPush")) != -1)
		{
			push = new GenderPush(gender);
			push->filter();
		}
		if (push->getIsFilter())
			isSomethingFiltred = true;
	}
	if (!isSomethingFiltred)
		cout << "-1";
	return 0;
}