#include "Push.h"
#include <iostream>
AbstractPush::AbstractPush()
{
//	string s = "";
//	getline(cin, s);
//	_text = s;
}

void AbstractPush::set_text(string s)
{
	_text = s;
}

void AbstractPush::set_type(string t)
{
	_type = t;
}

string AbstractPush::get_text()
{
	return _text;
}

string AbstractPush::get_type()
{
	return _type;
}

bool AbstractPush::getIsFilter()
{
	return _isFilter;
}

LocationPush::LocationPush(long time, float x_coord, float y_coord):AbstractPush()
{
	_time = time;
	_checkX = x_coord;
	_checkY = y_coord;
	getParametrs();

}

void LocationPush::findParametr(string s)
{
	int pos = 0;
	pos = s.find("x_coord");
	if (pos>=0)
	{
		pos += 8;
		string sHelper = &s[pos];
		_x_coord = atof(sHelper.c_str());
		return;
	}

	pos = s.find("y_coord");
	if (pos >= 0)
	{
		pos += 8;
		string sHelper = &s[pos];
		_y_coord = atof(sHelper.c_str());
		return;
	}

	pos = s.find("radius");
	if (pos >= 0)
	{
		pos += 6;
		string sHelper = &s[pos];
		_radius = atoi(sHelper.c_str());
		return;
	}

	pos = s.find("expiry_date");
	if (pos >= 0)
	{
		pos += 11;
		string sHelper = &s[pos];
		_expiry_date = atoi(sHelper.c_str());
		return;
	}

	pos = s.find("text");
	if (pos >= 0)
	{
		pos += 5;
		string sHelper = &s[pos];
		_text = sHelper;
		return;
	}
}

void LocationPush::getParametrs()
{
	string s = "";
	for (int i = 0; i < 5; i++)
	{
		getline(cin, s);
		findParametr(s);
		s="";
	}
}

void LocationPush::filter()
{
	if (_expiry_date < _time)
		_isFilter = true;
	if (sqrt(_checkX * _checkX + _checkY * _checkY) < _radius)
		_isFilter = true;
	
}

AgeSpecicPush::AgeSpecicPush(int age, long expity_date) :AbstractPush()
{
	_checkAge = age;
	_checkExpity_date = expity_date;
	getParametrs();

}

void AgeSpecicPush::findParametr(string s)
{
	int pos = 0;
	pos = s.find("age");
	if (pos >= 0)
	{
		pos += 4;
		string sHelper = &s[pos];
		_age = atoi(sHelper.c_str());
		return;
	}

	pos = s.find("expiry_date");
	if (pos >= 0)
	{
		pos += 11;
		string sHelper = &s[pos];
		_expiry_date = atoi(sHelper.c_str());
		return;
	}

	pos = s.find("text");
	if (pos >= 0)
	{
		pos += 5;
		string sHelper = &s[pos];
		_text = sHelper;
		return;
	}
}

void AgeSpecicPush::getParametrs()
{
	string s = "";
	for (int i = 0; i < 3; i++)
	{
		getline(cin, s);
		findParametr(s);
		s = "";
	}
}

void AgeSpecicPush::filter()
{
	if (_age < _checkAge)
		_isFilter = true;
	if (_expiry_date < _checkExpity_date)
		_isFilter = true;
}

TechPush::TechPush(int os_version) :AbstractPush()
{
	_checkOs_version = os_version;
	getParametrs();

}

void TechPush::findParametr(string s)
{
	int pos = 0;
	pos = s.find("os_version");
	if (pos >= 0)
	{
		pos += 11;
		string sHelper = &s[pos];
		_os_version = atoi(sHelper.c_str());
		return;
	}

	pos = s.find("text");
	if (pos >= 0)
	{
		pos += 5;
		string sHelper = &s[pos];
		_text = sHelper;
		return;
	}
}

void TechPush::getParametrs()
{
	string s = "";
	for (int i = 0; i < 2; i++)
	{
		getline(cin, s);
		findParametr(s);
		s = "";
	}
}

void TechPush::filter()
{
	if (_os_version < _checkOs_version)
		_isFilter = true;
}

LocationAgePush::LocationAgePush(float x_coord, float y_coord, int age) :AbstractPush()
{
	_checkAge = age;
	_checkX = x_coord;
	_checkY = y_coord;
	getParametrs();

}

void LocationAgePush::findParametr(string s)
{
	int pos = 0;
	pos = s.find("x_coord");
	if (pos >= 0)
	{
		pos += 8;
		string sHelper = &s[pos];
		_x_coord = atof(sHelper.c_str());
		return;
	}

	pos = s.find("y_coord");
	if (pos >= 0)
	{
		pos += 8;
		string sHelper = &s[pos];
		_y_coord = atof(sHelper.c_str());
		return;
	}

	pos = s.find("radius");
	if (pos >= 0)
	{
		pos += 6;
		string sHelper = &s[pos];
		_radius = atoi(sHelper.c_str());
		return;
	}

	pos = s.find("age");
	if (pos >= 0)
	{
		pos += 4;
		string sHelper = &s[pos];
		_age = atoi(sHelper.c_str());
		return;
	}

	pos = s.find("text");
	if (pos >= 0)
	{
		pos += 5;
		string sHelper = &s[pos];
		_text = sHelper;
		return;
	}
}

void LocationAgePush::getParametrs()
{
	string s = "";
	for (int i = 0; i < 5; i++)
	{
		getline(cin, s);
		findParametr(s);
		s = "";
	}
}

void LocationAgePush::filter()
{
	if (_age < _checkAge)
		_isFilter = true;
	if (sqrt(_checkX * _checkX + _checkY * _checkY) < _radius)
		_isFilter = true;
}

GenderAgePush::GenderAgePush(int age, char gender) :AbstractPush()
{
	_checkAge = age;
	_checkGender = _gender;
	getParametrs();

}

void GenderAgePush::findParametr(string s)
{
	int pos = 0;
	
	pos = s.find("gender");
	if (pos >= 0)
	{
		pos += 7;
		string sHelper = &s[pos];
		_gender = sHelper[pos];
		return;
	}

	pos = s.find("age");
	if (pos >= 0)
	{
		pos += 4;
		string sHelper = &s[pos];
		_age = atoi(sHelper.c_str());
		return;
	}

	pos = s.find("text");
	if (pos >= 0)
	{
		pos += 5;
		string sHelper = &s[pos];
		_text = sHelper;
		return;
	}
}

void GenderAgePush::getParametrs()
{
	string s = "";
	for (int i = 0; i < 3; i++)
	{
		getline(cin, s);
		findParametr(s);
		s = "";
	}
}

void GenderAgePush::filter()
{
	if (_age < _checkAge)
		_isFilter = true;
	if (_checkGender != _gender)
		_isFilter = true;
}

GenderPush::GenderPush(char gender) :AbstractPush()
{
	_checkGender = _gender;
	getParametrs();

}

void GenderPush::findParametr(string s)
{
	int pos = 0;

	pos = s.find("gender");
	if (pos >= 0)
	{
		pos += 7;
		string sHelper = &s[pos];
		_gender = sHelper[pos];
		return;
	}

	pos = s.find("text");
	if (pos >= 0)
	{
		pos += 5;
		string sHelper = &s[pos];
		_text = sHelper;
		return;
	}
}

void GenderPush::getParametrs()
{
	string s = "";
	for (int i = 0; i < 2; i++)
	{
		getline(cin, s);
		findParametr(s);
		s = "";
	}
}

void GenderPush::filter()
{
	if (_checkGender != _gender)
		_isFilter = true;
}